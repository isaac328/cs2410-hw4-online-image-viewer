package cs2410.assn4.controller;
import cs2410.assn4.model.*;

/**
 * The Controller class handles operations such as moving to the next
 * or previous image, adding an image and deleting an image
 * @author Alex Isaac
 * @version 1.0
 */
public class Controller
{
    /**
     * Model object for handling back-end operations (File I/O, List Maintenance)
     */
    private Model backend;
    /**
     * keeps track of the current index in the images ArrayList
     */
    private int currentIndex;
    /**
     * The max index we are able to access, so the size of ArrayList - 1
     */
    private int maxIndex;

    /**
     * Controller constructor, creates a controller object
     */
    public Controller()
    {
        backend = new Model();
        currentIndex = 0;
        maxIndex = backend.getImages().size() - 1;
    }

    /**
     * Moves to the next image in the list
     * @return the next CustomImage Object
     */
    public CustomImage nextImage()
    {
        //make sure its not the last picture
        if(currentIndex < maxIndex)
            return backend.getImages().get(++currentIndex);
        //if it is go to the front of the list
        else
        {
            currentIndex = 0;
            return backend.getImages().get(currentIndex);
        }
    }

    /**
     * Returns to the previous image in the list
     * @return the Previous CustomImage Object
     */
    public CustomImage prevImage()
    {
        //make sure we're not on the first pic
        if(currentIndex > 0)
            return backend.getImages().get(--currentIndex);
        //if we are go to the back of the list
        else
        {
            currentIndex = maxIndex;
            return backend.getImages().get(currentIndex);
        }
    }

    /**
     * Adds a new CustomImage to the list
     * @param url the url of the image
     * @param title the title of the image
     * @return the new CustomImage object
     */
    public CustomImage addImage(String url, String title)
    {
        backend.getImages().add(currentIndex, new CustomImage(url, title));
        maxIndex++;
        return backend.getImages().get(currentIndex);
    }

    /**
     * Deletes the current Image
     * @return the next CustomObject in the List
     */
    public CustomImage delImage()
    {
        //delete the image
        backend.getImages().remove(currentIndex);

        //if there are still images left
        if(backend.getImages().size() > 0)
        {
            //decrease size of list
            maxIndex--;
            //if that was the last index in the list, in which case currentIndex would be out of bounds
            if(currentIndex > maxIndex)
                currentIndex = maxIndex;
            //return the next image
            return backend.getImages().get(currentIndex);
        }
        //if it was the last image return the placeholder image
        else
        {
            maxIndex = 0;
            return new CustomImage("http://saveabandonedbabies.org/wp-content/uploads/2015/08/default.png",
                    "No Images");
        }
    }

    /**
     * Returns the current Image
     * @return the Current Image
     */
    public CustomImage currentImage()
    {
        //if there are images in the list return it
        if(backend.getImages().size() > 0)
            return backend.getImages().get(currentIndex);
        //if not then return the placeholder
        else
            return new CustomImage("http://saveabandonedbabies.org/wp-content/uploads/2015/08/default.png",
                    "No Images");
    }

    /**
     * Executed on close, saves the list
     */
    public void quit()
    {
        backend.saveList();
    }

    /**
     * Get the number of Images in the list
     * @return the number of Images in the list
     */
    public int getNumImages()
    {
        return backend.getImages().size();
    }
}

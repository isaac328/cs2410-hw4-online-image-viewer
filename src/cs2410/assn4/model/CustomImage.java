package cs2410.assn4.model;
import javafx.scene.image.Image;

/**
 * Object that contains the Image, url, and title of a picture
 * @author Alex Isaac
 * @version 1.0
 */
public class CustomImage
{
    /**
     * the picture
     */
    private Image picture;
    /**
     * the url of the picture
     */
    private String url;
    /**
     * the title of the picture
     */
    private String title;

    /**
     * Creates a CustomImage
     * @param url the url of the picture
     * @param title the title of the picture
     */
    public CustomImage(String url, String title)
    {
        this.title = title;
        this.url = url;
        picture = new Image(url, 550, 500, true, false);
    }

    /**
     * Getter for the picture
     * @return the picture
     */
    public Image getPicture()
    {
        return picture;
    }

    /**
     * Getter for the title
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Getter for the URL
     * @return the url
     */
    public String getUrl()
    {
        return url;
    }
}

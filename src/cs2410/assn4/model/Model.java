package cs2410.assn4.model;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class handles File I/O as well as the ArrayList that contains the CustomImages
 * @author Alex Isaac
 * @version 1.0
 */
public class Model
{
    /**
     * Holds all of the CustomImages
     */
    private ArrayList<CustomImage> images;
    /**
     * FileReader for reading in from file
     */
    private FileReader readerSource;
    /**
     * Scanner for reading in from file
     */
    private Scanner reader;
    /**
     * This FileWriter is used for saving the list. It deletes the contents
     * of the images.txt file and re-writes it with the current contents of the
     * images list
     */
    private FileWriter file;
    /**
     * This PrintWriter is used for re-writing the images.txt file
     */
    private PrintWriter writer;

    /**
     * Creates a Model Object
     */
    public Model()
    {
        try
        {
            //initialize things
            images = new ArrayList<>();
            readerSource = new FileReader("data/images.txt");
            reader = new Scanner(readerSource);
            //read in from file and throw it all in the ArrayList
            while(reader.hasNext())
            {
                String[] line = reader.nextLine().split(" ");
                images.add(new CustomImage(line[0], line[1]));
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.toString());
            //System.out.println("Error when initializing viewer");
        }
    }

    /**
     * Writes the list to the .txt file
     */
    public void saveList()
    {
        try
        {
            //initialize things, will delete contents of images.txt
            file = new FileWriter("data/images.txt");
            writer = new PrintWriter(file);

            //write each of the images to the file
            for(CustomImage img : images)
                writer.write(String.format("%s %s\n", img.getUrl(), img.getTitle()));

            //commit
            writer.close();
        }
        catch(Exception ex)
        {
            System.out.println("Image Unable to be Deleted");
        }
    }

    /**
     * Getter for the images list
     * @return images
     */
    public ArrayList<CustomImage> getImages()
    {
        return images;
    }
}

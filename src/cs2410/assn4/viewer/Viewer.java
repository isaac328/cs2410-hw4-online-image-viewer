package cs2410.assn4.viewer;

import cs2410.assn4.controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.text.*;
import javafx.stage.WindowEvent;
import java.util.Optional;

/**
 * @author Alex Isaac
 * @version 1.0
 *
 * This Class creates the GUI for the application
 * as well as handles event handlers and whatnot
 */
public class Viewer extends Application
{
    /**
     * Controller object to handle operations
     */
    private Controller controller = new Controller();
    /**
     * Y position for all the buttons
     */
    private int btnYPos = 500;
    /**
     * Initial x position for the buttons, aka the x position
     * of the leftmost button
     */
    private int btnXInitPos = 75;
    /**
     * Space between each of the buttons
     */
    private int btnXPosSpread = 100;

    /**
     * Creates the elements of the GUI
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        //declare all these first so we can use them
        Button delBtn = new Button();
        Button prevBtn = new Button();
        Button addBtn = new Button();
        Button nextBtn = new Button();

        Pane pane = new Pane();
        Scene scene1 = new Scene(pane, 550, 550);
        primaryStage.setScene(scene1);

        ImageView pic = new ImageView();
        pic.setY(40);
        pic.setImage(controller.currentImage().getPicture());
        pane.getChildren().add(pic);

        Text imgTitle = new Text();
        imgTitle.setFont(new Font(30));
        imgTitle.setX(pane.getWidth()/2 - 10);
        imgTitle.setY(30);
        imgTitle.setText(controller.currentImage().getTitle());
        pane.getChildren().add(imgTitle);

        prevBtn.setText("Previous Image");
        prevBtn.setLayoutX(btnXInitPos);
        prevBtn.setLayoutY(btnYPos);
        prevBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                if(controller.getNumImages() > 1)
                {
                    pic.setImage(controller.prevImage().getPicture());
                    imgTitle.setText(controller.currentImage().getTitle());
                }
            }
        });
        pane.getChildren().add(prevBtn);

        addBtn.setText("Add Image");
        addBtn.setLayoutX(btnXInitPos + (btnXPosSpread * 1) + 12);
        addBtn.setLayoutY(btnYPos);
        addBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                TextInputDialog input = new TextInputDialog();
                input.setHeaderText("Enter URL");
                input.setTitle("Hello");
                Optional<String> url = input.showAndWait();
                if(url.isPresent())
                {
                    input.setHeaderText("Enter Title");
                    input.getEditor().clear();
                    Optional<String> title = input.showAndWait();
                    if(title.isPresent())
                    {
                        pic.setImage(controller.addImage(url.get(), title.get()).getPicture());
                        imgTitle.setText(controller.currentImage().getTitle());

                        //if we just added a pic to an empty list, re-enable buttons
                        if(controller.getNumImages() == 1)
                        {
                            prevBtn.setDisable(false);
                            delBtn.setDisable(false);
                            nextBtn.setDisable(false);
                        }
                    }
                }
            }
        });
        pane.getChildren().add(addBtn);

        nextBtn.setText("Next Image");
        nextBtn.setLayoutX(btnXInitPos + (btnXPosSpread * 3));
        nextBtn.setLayoutY(btnYPos);
        nextBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                if(controller.getNumImages() > 1)
                {
                    pic.setImage(controller.nextImage().getPicture());
                    imgTitle.setText(controller.currentImage().getTitle());
                }
            }
        });
        pane.getChildren().add(nextBtn);

        delBtn.setText("Delete Image");
        delBtn.setLayoutX(btnXInitPos + (btnXPosSpread * 2));
        delBtn.setLayoutY(btnYPos);
        delBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent event)
            {
                if(controller.getNumImages() > 0)
                {
                    pic.setImage(controller.delImage().getPicture());
                    imgTitle.setText(controller.currentImage().getTitle());
                }

                //if that was the last one disable buttons
                if(controller.getNumImages() == 0)
                {
                   prevBtn.setDisable(true);
                   delBtn.setDisable(true);
                   nextBtn.setDisable(true);
                }
            }
        });
        pane.getChildren().add(delBtn);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event)
            {
                controller.quit();
            }
        });

        primaryStage.show();
    }
}
